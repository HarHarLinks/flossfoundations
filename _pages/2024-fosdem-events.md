---
title: "2024 FOSDEM Events"
layout: single
permalink: /2024-fosdem-events
---

What's happening during and around FOSDEM 2024?

Add events here, ordered by date, and include the datetime, location, and a link to the event.

You can update the list by sending a merge request for [this file in the website repository](https://gitlab.com/flossfoundations/flossfoundations/-/blob/master/_pages/2024-fosdem-events.md).

<!--
* [event name](link)
    * date and time
    * location
-->
## Monday, 29 January 2024

<!--
* [event name](link)
    * date and time
    * location
-->

## Tuesday, 30 January 2024

<!--
* [event name](link)
    * date and time
    * location
-->

## Wednesday, 31 January 2024

* [Mautic Community Sprint](https://community.mautic.org/assemblies/community-team/f/5/meetings/79)
    * 31 Jan - 1 Feb 2024
    * Dropsolid, Moutstraat 60, 9000 Gent, Belgium

## Thursday, 01 February 2024

* [Mautic Community Sprint](https://community.mautic.org/assemblies/community-team/f/5/meetings/79)
    * 31 Jan - 1 Feb 2024
    * Dropsolid, Moutstraat 60, 9000 Gent, Belgium
* [CentOS Connect](https://connect.centos.org/)
    * 01-02 February, 2024
    * Royal Room at Radisson Collection Hotel, Grand Place Brussels, 47 Rue du Fossé aux Loups, 1000 Brussels, Belgium
* [Software Heritage Symposium](https://softwareheritage.org)
    * 01 February, 2024
    * UNESCO Headquarters, 2 place de Fontenoy, Paris, France
* [CHAOSScon EU](https://chaoss.community/chaosscon-2024-eu/)
    * 01 February, 2024
    * Beford Hotel and Congress Centre, Rue du Midi 135 Brussels, 1000, Belgium

## Friday, 02 February 2024

* [CentOS Connect](https://connect.centos.org/)
    * 01-02 February, 2024
    * Royal Room at Radisson Collection Hotel, Grand Place Brussels, 47 Rue du Fossé aux Loups, 1000 Brussels, Belgium

* [FOSS license and security compliance tools workshop](https://opencollective.com/aboutcode/events/fosdem-2024-fringe-workshop-on-foss-license-and-security-compliance-tools-ea75e63c#category-ABOUT)
    * 02 February, 2024
    * Location TBA

* [EU Open Source Policy Summit 2024](https://summit.openforumeurope.org/)
    * 02 February, 2024
    * Royal Flemish Theater (KVS), Arduinkaai 7, 1000 Brussels, Belgium

## Saturday, 03 February 2024 

* [FOSDEM!](https://fosdem.org/2024/)
    * 03-04 February, 2024
    * Université libre de Bruxelles, Campus du Solbosch, Avenue Franklin D. Roosevelt 50, 1050 Bruxelles, Belgium ([transportation](https://fosdem.org/2024/practical/transportation/))

<!--
* [event name](link)
    * date and time
    * location
-->

## Sunday, 04 February 2024

* [FOSDEM!](https://fosdem.org/2024/)
    * 03-04 February, 2024
    * Université libre de Bruxelles, Campus du Solbosch, Avenue Franklin D. Roosevelt 50, 1050 Bruxelles, Belgium ([transportation](https://fosdem.org/2024/practical/transportation/))

<!--
* [event name](link)
    * date and time
    * location
-->

## Monday, 05 February 2024

<!--
* [event name](link)
    * date and time
    * location
-->

## Tuesday, 06 February 2024

* [State of Open Conference](https://stateofopencon.com)
    * 06-07 February, 2024
    * The Brewery in London, UK.  The venue is located at [52 Chiswell Street, London, EC1Y 4SD](https://stateofopencon.com/venue-travel-information-2024/).

## Wednesday, 07 February 2024

* [State of Open Conference](https://stateofopencon.com)
    * 06-07 February, 2024
    * The Brewery in London, UK.  The venue is located at [52 Chiswell Street, London, EC1Y 4SD](https://stateofopencon.com/venue-travel-information-2024/).
