---
layout: single
title: "FOSDEM 2012"
date: 2012-02-03
permalink: /fosdem-2012
categories:
    - meetings
---

The face-to-face meetings are a place for the people who try to Get Stuff Done at various open source projects and non-profits around open source to congregate, share information, and pick each others' brains.

## When and Where:

Friday, February 3, 7:45pm, at [FOSDEM](http://fosdem.org/2012/).

* [Brasserie Jaloa](http://web.archive.org/web/20120120174918/http://www.jaloa.com/)
    * 5-7 Place Sainte Catherine
    * 1000 Brussels
    * + 32 02 512 1831

Potential Attendees (Friday evening)

* Donnie Berkholz, Gentoo Foundation
* Lydia Pintscher, KDE
* Simon Phipps, OSI (after 8pm)
* Mark Taylor
* Allison Randal, TPF, PSF, Ubuntu
* Dave Neary, GNOME Foundation et al
* Clémentine Valayer
* Gijs Hillenius, JoinUp
* Claudia Rauch, KDE
* Deborah Nicholson, Eximious
* Peter Saint-Andre
* Stefano Zacchiroli, Debian
* Karanbir Singh
* Peter Linnell, Scribus
* Charles-H. Schulz, LibreOffice
* Mike Milinkovich, Eclipse
* Florian Effenberger, LibreOffice
* Nick Barcet, Ubuntu

Potential Attendees (Saturday evening)

* Loïc Dachary, APRIL.org, FSF France
* Dave Neary, GNOME Foundation (maybe)

## Topics:

* Food
* Drink
* Camaraderie
* Others?

Thank you all who can attend. Please contribute your notes as appropriate.

Location ideas

* http://www.restaurant-rose-blanche.be/en/welcome/ it's near Grande Place plus has vegetarian options, beer, good reviews and is "good for groups"
